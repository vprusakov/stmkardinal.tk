const htmlNode = document.documentElement;
const bodyNode = document.body;

var adjustUI = (function() {
  const pageNode = document.getElementById("page");
  const mobileDropdown = document.getElementById("mobile-dropdown");
  const burgerIcon = document.getElementById("burger-icon");
  const navListSubs = document.getElementsByClassName("js-nav-list-sub");
  const headerDropdowns = document.getElementsByClassName("js-header-dropdown");

  var savedScrollY = null;
  var menu = {
    isOpen: false
  };

  var bindUI = function() {
    burgerIcon.addEventListener("click", toggleMobileMenu);

    for (var i = 0; i < navListSubs.length; i++) {
      navListSubs[i].addEventListener("click", function() {
        openSubList(this, navListSubs);
      });
    }
    for (var i = 0; i < headerDropdowns.length; i++) {
      headerDropdowns[i].addEventListener("click", function() {
        openSubList(this, headerDropdowns);
      });
    }
  };

  var openSubList = function(subnav, subnavs) {
    subnav.parentNode.classList.contains("active")
      ? subnav.parentNode.classList.remove("active")
      : (closeSubLists(subnavs), subnav.parentNode.classList.add("active"));
  };

  var closeSubLists = function(subnavs) {
    for (var i = 0; i < subnavs.length; i++) {
      subnavs[i].parentNode.classList.remove("active");
    }
  };

  var toggleMobileMenu = function(e) {
    e.preventDefault();
    menu.isOpen ? closeMobileMenu() : openMobileMenu();
  };

  var openMobileMenu = function() {
    menu.isOpen = true;
    savePageScroll();
    htmlNode.classList.add("nav-active");
    pageNode.style.height = document.documentElement.clientHeight + "px";
    mobileDropdown.style.height = document.documentElement.clientHeight + "px";
    pageNode.scrollTo(0, savedScrollY);
  };

  var closeMobileMenu = function() {
    menu.isOpen = false;
    pageNode.style.height = "auto";
    mobileDropdown.style.height = "0";
    window.scrollTo(0, savedScrollY);
    htmlNode.classList.remove("nav-active");
    //nav.scrollTop = 0;
    closeSubLists(headerDropdowns);
  };

  var savePageScroll = function() {
    savedScrollY = window.scrollY;
  };

  return {
    init: function() {
      bindUI();
    }
  };
})();

adjustUI.init();
