var catalog = (function(e) {
  var n,
    a = {
      grid: ".catalog-grid",
      gridElement: ".grid-item",
      gridSizer: ".catalog-grid__grid-sizer"
      //   loader: ".loader",
    };
  return {
    init: function() {
      catalog.events();
    },
    events: function() {
      e(a.grid).imagesLoaded(function() {
        catalog.initMasonry(e(a.grid));
      });
    },
    initMasonry: function(t) {
      n = t.masonry({
        itemSelector: a.gridElement,
        columnWidth: a.gridSizer,
        percentPosition: true,
        gutter: 24
      });
    }
  };
})(jQuery);
document.addEventListener("DOMContentLoaded", catalog.init);
