const gulp = require("gulp");
const postcss = require("gulp-postcss");
const cssimport = require("postcss-import");
const postcssPresetEnv = require("postcss-preset-env");
const cssnano = require("cssnano");

const imagemin = require("gulp-imagemin");
const imageminMozjpeg = require("imagemin-mozjpeg");
const imageResize = require("gulp-image-resize");

gulp.task("watch", function() {
  gulp.watch("./src/css/**/*.css", gulp.series("css"));
});

gulp.task("images-for-galleries", () =>
  gulp
    .src("assets/images/source/s/products/kitchen/gallery5/*.jpg")
    .pipe(
      imageResize({
        height: 1080,
        width: 1920
      })
    )
    .pipe(
      imagemin([
        imageminMozjpeg({
          quality: 75
        })
      ])
    )
    .pipe(gulp.dest("assets/images/s/products/kitchen/gallery5/"))
);

gulp.task("images-for-covers", () =>
  gulp
    .src("assets/images/source/s/products/kitchen/covers/*.jpg")
    .pipe(
      imageResize({
        width: 480
      })
    )
    .pipe(
      imagemin([
        imageminMozjpeg({
          quality: 75
        })
      ])
    )
    .pipe(gulp.dest("assets/images/s/products/kitchen/covers/"))
);

gulp.task("normal-images", () =>
  gulp
    .src("assets/images/source/s/furniture/classes/designer/*.jpg")
    .pipe(
      imageResize({
        width: 1440,
        height: 800
      })
    )
    .pipe(
      imagemin([
        imageminMozjpeg({
          quality: 75
        })
      ])
    )
    .pipe(gulp.dest("assets/images/s/furniture/classes/designer/"))
);

gulp.task("css", function() {
  var processors = [
    cssimport({
      from: "./src/css/styles.css"
    }),
    postcssPresetEnv({
      stage: 3,
      browsers: "defaults",
      features: { "nesting-rules": true, "custom-media-queries": true }
    }),
    cssnano()
  ];
  return gulp
    .src("./src/css/**/*.css")
    .pipe(postcss(processors))
    .pipe(gulp.dest("./src/built/styles"));
});
