<?php

    return [
        'title' => 'Галерея',
        'show_in_templates' => [ 7 ],
        'container' => 'section-large',

        'templates' => [
            'owner' =>
                '<div class="content-block">
                    <div class="photo-gallery row pswp-gallery" data-pswp-uid="1">
                        [+gallery+]
                    </div>
                </div>',
            'gallery' =>
                '<figure class="photo-gallery__item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <a class="link-block" href="[+image+]">
                        <img class="adaptive" src="[[phpthumb? &input=`[+image+]` &options=`w=480,h=300,zc=1`]]">
                    </a>
                    <figcaption class="photo-gallery__caption">[+caption+]</figcaption>
                </figure>',
        ],

        'fields' => [
            'gallery' => [
                'caption' => 'Gallery',
                'type'    => 'group',
                'fields'  => [
                    'image' => [
                        'caption' => 'Image',
                        'type'    => 'image',
                    ],
                    'caption' => [
                        'caption' => 'Подпись',
                        'type'    => 'text'   
                    ]
                ],
            ],
        ],
];