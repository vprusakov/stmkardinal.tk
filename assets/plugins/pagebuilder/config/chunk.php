<?php

    return [
        'title' => 'Чанк',
        'show_in_templates' => [ 4, 7 ],
        'container' => 'section-large',

        'templates' => [
            'owner' =>
                '<div class="content-block">
                    [+text+]
                </div>'
        ],

        'fields' => [
            'text' => [
                'caption' => 'Чанк',
                'type'    => 'text',
            ]
        ]
    ];