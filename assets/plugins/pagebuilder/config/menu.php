<?php
return [
    'title' => 'Меню',
    'show_in_templates' => [ 7 ],
    'container' => 'section-large',

    'templates' => [
        'owner' =>
            '<div class="content-block">
                <div class="row content-items">
                    [!DocLister?
                        &display=`999`
                        &order=`ASC`
                        &tvList=`outer-card-image`
                        &tpl=`@CODE:<div class="[!colSize? &value=`[+radio+]`!]">
                            <a href="[~[+id+]~]" class="content-item">
                                <div class="content-item__illustration" style="background-image: url([+tv.outer-card-image+]);"></div>
                                <div class="content-item__text"><span class="content-item__text__label">[+pagetitle+]</span></div>
                            </a>
                        </div>`
                    !]
                </div>
            </div>'
    ],

    'fields' => [
        'radio' => [
            'caption'  => 'Radio',
            'type'     => 'radio',
            'layout'   => 'horizontal',
            'elements' => 'Half==0||Third==1||Fourth==2',
            'default'  => 2,
        ],
    ],
];
?>