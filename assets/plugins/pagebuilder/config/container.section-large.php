<?php

    return [
        'title' => 'Секция',
        'placement' => 'tab',
        'templates' => [
            'owner' =>
                '<section class="section section--large">
                    <div class="section-content">
                        [+wrap+]
                    </div>
                </section>',
        ],
    ];