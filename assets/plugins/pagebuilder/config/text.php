<?php

    return [
        'title' => 'Текст',
        'show_in_templates' => [ 4, 7, 16, 19 ],
        'container' => 'section-large',

        'templates' => [
            'owner' =>
                '<div class="content-block">
                    [+richtext+]
                </div>'
        ],

        'fields' => [
            'richtext' => [
                'caption' => 'Текст',
                'type'    => 'richtext',
                'theme'   => 'editor',
                'options' => [
                    'height' => '300px',
                ]
            ]
        ]
    ];