<?php

    return [
        'title' => 'All fields',

        'show_in_templates' => [ 7 ],

//        'show_in_docs' => [ 2 ],

//        'hide_in_docs' => [ 5 ],

//        'order' => 1,

       'container' => 'section-large',

        'templates' => [
            'owner' => '
                <div>Radio:<br> [+radio+]</div>
            '
        ],

        'fields' => [
            'radio' => [
                'caption'  => 'Radio',
                'type'     => 'radio',
                'layout'   => 'horizontal',
                'elements' => 'Half==0||Third==1||Fourth==2',
                'default'  => 1,
            ],
        ],
    ];
?>