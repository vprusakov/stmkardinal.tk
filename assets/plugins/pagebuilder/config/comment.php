<?php

    return [
        'title' => 'Блок с отзывом',

        'show_in_docs' => [ 200 ],

        'container' => 'default',

        'templates' => [
            'owner' => '<div class="row">
                <div class="col-lg-offset-3 col-lg-6 col-sm-offset-2 col-sm-8">
                    <div class="comment">
                        <p class="comment__header"><span class="comment__name">[+name+]</span><span class="comment__date">[+date+]</span></p>
                        [+richtext+]
                    </div>
                </div>
            </div>'
        ],

        'fields' => [
            'name' => [
                'caption' => 'Имя',
                'type'    => 'text',
            ],
            'date' => [
                'caption' => 'Дата',
                'type'    => 'date',
            ],
            'richtext' => [
                'caption' => 'Отзыв',
                'type'    => 'richtext',
                'theme'   => 'inline',
                'options' => [
                    'height' => '180px',
                ]
            ]
        ]
    ];