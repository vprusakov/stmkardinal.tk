<?php
$settings['display'] = 'horizontal';
$settings['fields'] = array(
    'height' => array(
        'caption' => 'Высота',
        'type' => 'text',
        'width' => '150'
    ),
    'width' => array(
        'caption' => 'Ширина',
        'type' => 'text',
        'width' => '150'
    ),
    'depth' => array(
        'caption' => 'Глубина',
        'type' => 'text',
        'width' => '150'
    ),
    'price' => array(
        'caption' => 'Цена комплекта',
        'type' => 'text',
        'width' => '150'
    )
);
$settings['templates'] = array(
    'outerTpl' => '[+wrapper+]',
    'rowTpl' => '<p>[+height+] x [+width+] x [+depth+] - [+price+]</p>'
);
