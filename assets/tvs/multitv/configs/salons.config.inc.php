<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'name' => array(
        'caption' => 'Название',
        'type' => 'text'
    ),
    'address' => array(
        'caption' => 'Адрес',
        'type' => 'textarea'
    ),
    'coords' => array(
        'caption' => 'Координаты',
        'type' => 'text'
    ),
    'phone' => array(
        'caption' => 'Телефон',
        'type' => 'text'
    ),
    'email' => array(
        'caption' => 'E-mail',
        'type' => 'text'
    ),
    'time' => array(
        'caption' => 'Время работы',
        'type' => 'text'
    ),
);
$settings['templates'] = array(
    'outerTpl' => '<div class="list_salons">[+wrapper+]</div>',
    'rowTpl' => '<div class="col-sm-4"><a rel="var1" href="[+image+]"><img class="img-responsive" src="[[phpthumb? &input=`[+image+]` &options=`w=720,h=507,zc=1`]]"></a></div>'
);
