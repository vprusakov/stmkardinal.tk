<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'image' => array(
        'caption' => 'Фотография',
        'type' => 'image'
    ),
    'thumb' => array(
        'caption' => 'Превью',
        'type' => 'thumb',
        'thumbof' => 'image'
    ),
    'caption' => array(
        'caption' => 'Подпись к фотографии',
        'type' => 'text'
    ),
);
$settings['templates'] = array(
    'outerTpl' => '<div class="photo-gallery row pswp-gallery">[+wrapper+]</div>',
    'rowTpl' =>
    '<figure class="photo-gallery__item col-lg-4 col-md-6 col-sm-6 col-xs-12" data-pswp-item-index="[+row.number+]">
        <a class="link-block" href="[+image+]">
            <img class="adaptive" src="[[phpthumb? &input=`[+image+]` &options=`w=480,h=300,zc=1`]]">
        </a>
        <figcaption class="photo-gallery__caption">[+caption+]</figcaption>
    </figure>'
);
?>