<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'image' => array(
        'caption' => 'Source path',
        'type' => 'image'
    ),
    'thumb' => array(
        'caption' => 'Превью',
        'type' => 'thumb',
        'thumbof' => 'image'
    ),
    'title' => array(
        'caption' => 'Title attribute',
        'type' => 'text'
    ),
    'alt' => array(
        'caption' => 'Alt attribute',
        'type' => 'text'
    )

);
$settings['templates'] = array(
    'outerTpl' => '<div class="swiper-wrapper">((wrapper))</div>',
    'rowTpl' => '<div class="swiper-slide">
        <img class="swiper-slide-image" src="[[phpthumb? &input=`[+image+]` &options=`fltr[]=wmi|/assets/img/watermark.png|BR|30|40|40`]]" title="[+title+]" alt="[+alt+]">
    </div>'
    );
?>

