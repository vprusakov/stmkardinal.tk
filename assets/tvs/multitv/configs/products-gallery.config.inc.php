<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'image' => array(
        'caption' => 'Картинка',
        'type' => 'image'
    ),
    'thumb' => array(
        'caption' => 'Превью',
        'type' => 'thumb',
        'thumbof' => 'image'
    )
);
$settings['templates'] = array(
    'outerTpl' => '[+wrapper+]',
    'rowTpl' =>
       '<div class="pg__nav-item">
            <button class="nav-item__inner o-button" style="background-image: url([[phpthumb? &input=`[+image+]` &options=`w=100,h=70,zc=1`]])">
                <span class="nav-item__number">[+row.number+]</span>
            </button>
        </div>'
    );
?>