<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'height' => array(
        'caption' => 'Высота',
        'type' => 'text'
    ),
    'width' => array(
        'caption' => 'Ширина',
        'type' => 'text'
    ),
    'depth' => array(
        'caption' => 'Глубина',
        'type' => 'text'
    ),
    'price' => array(
        'caption' => 'Телефон',
        'type' => 'text'
    ),
);
$settings['templates'] = array(
    'outerTpl' => '[+wrapper+]',
    'rowTpl' => '<p>[+height+] x [+width+] x [+depth+] - [+price+]</p>'
);
