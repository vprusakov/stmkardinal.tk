<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'image' => array(
        'caption' => 'Картинка',
        'type' => 'image'
    ),
    'thumb' => array(
        'caption' => 'Превью',
        'type' => 'thumb',
        'thumbof' => 'image'
    ),
    'price' => array(
        'caption' => 'Цена',
        'type' => 'text',
    ),
);
$settings['templates'] = array(
    'outerTpl' => '<div class="row"><div class="gallery-container">[+wrapper+]</div></div>',
    'rowTpl' => '<div class="gallery-item col-lg-4 col-md-6 col-sm-6 col-xs-12" data-pswp-item-index="[+row.number+]"><a class="gallery" href="[+image+]"><img class="adaptive-gallery-img" src="[[phpthumb? &input=`[+image+]` &options=`w=600,h=400,zc=C`]]" alt="[+pagetitle+]"></a><span class="gallery-item-price">[+price+] ₽</span></div>',
);
?>