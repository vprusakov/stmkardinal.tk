<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'image' => array(
        'caption' => 'Картинка',
        'type' => 'image'
    ),
    'thumb' => array(
        'caption' => 'Превью',
        'type' => 'thumb',
        'thumbof' => 'image'
    ),
    'link' => array(
        'caption' => 'Ссылка',
        'type' => 'link'
    ),
    'text' => array(
        'caption' => 'Текст',
        'type' => 'text'
    ),
);
$settings['templates'] = array(
    'outerTpl' => '<div class="catalog-grid"><div class="grid-sizer catalog-grid__grid-sizer"></div>[+wrapper+]</div>',
    'rowTpl' =>
    '<div class="grid-item catalog-grid__item">
        <a class="link-block" href="/[+link+]">
            <img class="catalog-grid__image" src="[+image+]">
            <div class="catalog-grid__overlay">
                <span class="catalog-grid__text">[+text+]</span>
            </div>
        </a>
    </div>'
);
?>